package id.ac.ui.cs.advprog.tutorial1.strategy;

public class Swim implements SwimBehavior{
    @Override
    public void swim() {
        System.out.println("Semua bebek bisa ngapung (padahal perintahnya berenang)");
    }
}
